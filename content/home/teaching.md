+++
# Custom widget.
# An example of using the custom widget to create your own homepage section.
# To create more sections, duplicate this file and edit the values below as desired.
widget = "customsx"
active = true
date = 2016-04-20T00:00:00

# Se inserita immagine, visualizza immagine
image = "https://res.cloudinary.com/pdare/image/upload//v1532708201/computer.jpg"

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Titolo sezione uno"
subtitle = ""

# Order that this section will appear in.
weight = 5

+++

This is an example of using the *custom* widget to create your own homepage section.

To remove this section, either delete `content/home/teaching.md` or edit the frontmatter of the file to deactivate the widget by setting `active = false`.
