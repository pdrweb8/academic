+++
authors = ["paolo"]
categories = []
date = "2018-07-29T14:47:18.000+02:00"
summary = "Breve promemoria per la configurazione iniziale del sito e personalizzazioni effettuate."
tags = ["Academic", "Hugo"]
title = "Configurazione iniziale del sito"
[header]
caption = ""
image = "https://res.cloudinary.com/pdare/image/upload/v1532892544/siti/banner/computer.jpg"
preview = true

+++
{{% toc %}}

## Installazione veloce tramite browser, Netlify e GitLab

1. [Installazione di Academic tramite Netlify](https://app.netlify.com/start/deploy?repository=https://github.com/sourcethemes/academic-kickstart)
   * Netlify ti fornirà un URL personalizzabile per accedere al tuo nuovo sito
2. Su GitLab, vai al nuovo repository creato `academic-kickstart` ed edita il file `config.toml` per personalizzare il sito. Appena aggiornato il file, il sito verrà automaticamente aggiornato.

### Clonazione del sito in locale per la personalizzazione con `Sublime Text`

Da riga comando eseguire le seguenti istruzioni:

```python
git clone https://gitlab.com/id-utente/academic.git
cd academic
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

### Aggiornamento del tema

Per impostazione predefinita, Academic è installato come Git sub-module che può essere aggiornato eseguendo il seguente comando da riga comando:

```python
git submodule update --remote --merge
```

### Aggiornamento del sito

Dopo ogni modifica apportata, è possibile aggiornare il sito eseguendo i seguenti comandi Git (da riga comando):

```python
git status  
git add *
git commit - m "descrizione del commit"
git push -u origin master
```

## Personalizzazioni eseguite

## Note particolari

### Dimensioni delle immagini per i post

Le immagini per la copertina dei post devono avere la dimensione di 1024x300. Da verificare come renderli responsive

Inserimento riga da CMS

### Traduzione delle stringhe in italiano tramite la cartella i18n

Parametri da impostare nel file config.toml:

```python
# Default language to use (if you setup multilingual support)
defaultContentLanguage = "it"
# Configura la versione in italiano del sito.
[Languages.it]
  languageCode = "it"
```